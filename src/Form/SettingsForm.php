<?php

namespace Drupal\trinion_tel\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;

/**
 * Configure Trinion telephony settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trinion_tel_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['trinion_tel.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['trinion_tel_api_token'] = [
      '#type' => 'textfield',
      '#title' => 'Секретный ключ для вызова методов API',
      '#default_value' => $this->config('trinion_tel.settings')->get('trinion_tel_api_token'),
    ];
    
    $form['trinion_tel_ats_url'] = [
      '#type' => 'textfield',
      '#title' => 'Адрес или доменное имя Asterisk',
      '#default_value' => $this->config('trinion_tel.settings')->get('trinion_tel_ats_url'),
      '#description' => 'С указанием схемы http/https',
    ];
    $user = $this->config('trinion_tel.settings')->get('trinion_tel_default_responsible');
    if ($user)
      $user = User::load($user);
    $form['trinion_tel_default_responsible'] = [
      '#type' => 'entity_autocomplete',
      '#title' => 'Ответственный по умолчанию',
      '#description' => 'Будет подставлен в поле Ответственный пользователь в не отвеченном звонке',
      '#default_value' => $user,
      '#target_type' => 'user',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('trinion_tel.settings')
      ->set('trinion_tel_api_token', $form_state->getValue('trinion_tel_api_token'))
      ->set('trinion_tel_ats_url', $form_state->getValue('trinion_tel_ats_url'))
      ->set('trinion_tel_default_responsible', $form_state->getValue('trinion_tel_default_responsible'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
