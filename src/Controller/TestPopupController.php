<?php

namespace Drupal\trinion_tel\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Trinion telephony routes.
 */
class TestPopupController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    return [
      [
        '#type' => 'submit',
        '#id' => 'test-open-new-pickup-popup',
        '#value' => t('New pickup popup'),
      ]
    ];
  }

}
