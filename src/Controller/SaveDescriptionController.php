<?php

namespace Drupal\trinion_tel\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\node\Entity\Node;

/**
 * Returns responses for Trinion telephony routes.
 */
class SaveDescriptionController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {
    $request = \Drupal::request();
    if ($call_id = $request->get('call_id')) {
      if ($node = Node::load($call_id)) {
        $node->field_tl_text = $request->get('text');
        $node->save();
      }
    }
    return new \Symfony\Component\HttpFoundation\JsonResponse();
  }

}
