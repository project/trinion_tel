<?php

namespace Drupal\trinion_tel\Services;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Telephony service.
 */

const CALL_TYPE_INCOMING = 1;

class Telephony {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a Telephony object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Create call entity
   */
  public function createCallEntity($uid, $phone_number, $type, $contact_id = NULL) {
    $values = [
      'title' => $this->callTypeName($type) . " {$phone_number}",
      'uid' => $uid,
      'langcode' => 'en',
      'status' => '1',
      'type' => 'zvonok',
      'field_tt_call_type' => [
        'value' => $type == 2 || $type == 3 ? 'in' : 'out',
      ],
      'field_tl_nomer_telefona' => $phone_number,
    ];

    if ($contact_id)
      $values['field_tt_call_from']['target_id'] = $contact_id;

    $call = $this->entityTypeManager->getStorage('node')->create($values);
    $call->save();
    return $call;
  }

  /**
   * Finish call
   */
  public function closeCallEntity($nid, $duration, $failed_reason, $record_url, $uid, $code) {
    $call = $this->entityTypeManager->getStorage('node')->load($nid);
    $call->field_tt_dlitelnost = $duration;
    $call->field_tt_record_url = $record_url;
    $call->field_tt_otvetstvennyy = $uid ? $uid : \Drupal::config('trinion_tel.settings')->get('trinion_tel_default_responsible');
    if ($code != 200)
      $call->field_tt_propuschennyy_zvokon = TRUE;
    $call->save();
    return $call;
  }

  /**
   * User freandly call type name
   * @param $id
   * @return string
   */
  public function callTypeName($id) {
    $types = [
      1 => 'исходящий',
      2 => 'входящий',
      3 => 'входящий с перенаправлением',
      4 => 'обратный',
    ];
    return isset($types[$id]) ? $types[$id] : $id;
  }

}
