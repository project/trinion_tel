<?php

namespace Drupal\trinion_tel\Plugin\rest\resource;

use Bloatless\WebSocket\PushClient;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Session\AccountProxy;
use Drupal\node\Entity\Node;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\user\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Represents TelephonyStartCall records as resources.
 *
 * @RestResource (
 *   id = "trinion_tel_telephonystartcall",
 *   label = @Translation("TelephonyStartCallResource"),
 *   uri_paths = {
 *     "canonical" = "/rest/telephony.start_call/{node}",
 *   }
 * )
 *
 * @DCG
 * This plugin exposes database records as REST resources. In order to enable it
 * import the resource configuration into active configuration storage. You may
 * find an example of such configuration in the following file:
 * core/modules/rest/config/optional/rest.resource.entity.node.yml.
 * Alternatively you can make use of REST UI module.
 * @see https://www.drupal.org/project/restui
 * For accessing Drupal entities through REST interface use
 * \Drupal\rest\Plugin\rest\resource\EntityResource plugin.
 */
class TelephonyStartCall extends ResourceBase {

  /**
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var Node
   */
  protected $client;

  /**
   * @var Node
   */
  protected $call;

  /**
   * @var User
   */
  protected $user;

  /**
   * @var string
   */
  protected $phoneNumber;

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats,  LoggerInterface $logger, EntityTypeManager $entity_type_manager, AccountProxy $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger, $entity_type_manager);
    $this->entityTypeManager = $entity_type_manager;
    $this->user = $entity_type_manager->getStorage('user')->load($current_user->id());
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('logger'),
      $container->get('entity_type.manager'),
      $container->get('current_user')
    );
  }

  /**
   * Responds to POST requests and saves the new record.
   *
   * @param mixed $data
   *   Data to write into the database.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function get($node) {
    if ($response = $this->validate($node)) {
      return new ModifiedResourceResponse($response, 200);
    }

    $call = \Drupal::service('trinion_tel.telephony')->createCallEntity(0, $this->phoneNumber, 1, empty($this->client) ? NULL : $this->client->id());
    if ($call) {

      // Send POST request to asterisk
      $config = \Drupal::config('trinion_tel.settings');
      $data = [
        'event' => 'ONEXTERNALCALLSTART',
        'USER_PHONE_INNER' => $this->user->get('field_tt_user_phone_inner')->getString(),
        'CALL_ID' => $call->id(),
        'PHONE_NUMBER' => $this->phoneNumber,
        'TOKEN' => $config->get('trinion_tel_api_token')
      ];

      $request = \Drupal::httpClient()->post($config->get('trinion_tel_ats_url') . '/admin/modules/asdrupal/api/index.php', ['form_params' => $data]);
      $result = $request->getBody()->getContents();

      if ($result != 'OK') {
        $response['errors'][] = 'Ошибка телефонии';
        //\Drupal::logger('trinion_tel')->log(1, 'Ошибка телефонии: ' . $result);
        return new ModifiedResourceResponse($response, 200);
      }

      if (!empty($this->client)) {
        $company = self::getCompany($this->client);
        if ($company)
          $company_id = $company->id();
        elseif ($this->client->bundle() == 'kompanii') {
          $company_id = $this->client->id();
        }
      }


      $pushClient = new PushClient(realpath(__DIR__ . '/../../../../wserver/phpwss.sock'));
      $pushClient->sendToApplication('ws', [
        'action' => 'NewCallPopup',
        'data' => [
          'title' => 'Исходящий звонок',
          'type' => 'outgoing',
          'callId' => $call->id(),
          'uid' => $this->user->id(),
          'clientName' => empty($this->client) ? '' : $this->client->label(),
          'companyName' => $company ? $company->label() : '',
          'companyId' => $company_id ?? '',
          'clientId' => empty($this->client) ? 0 : $this->client->id(),
          'phoneNumber' => $this->phoneNumber,
        ],
      ]);
    }
    return new ModifiedResourceResponse([], 200);
  }

  /**
   * Validate incoming data
   * @param $data
   */
  public function validate($node) {
    $errors = [];
    $node = $this->entityTypeManager->getStorage('node')->load($node);
    if (!$node)
      $errors[] = 'Неизвестный покупатель';
    elseif (in_array($node->bundle(), ['kompanii', 'contact', 'lead', 'zvonok',])) {
      $phone_number = $node->get('field_tl_nomer_telefona')->getString();
    }
    if (!empty($phone_number)) {
      if (in_array($node->bundle(), ['kompanii', 'contact', 'lead', ]))
        $this->client = $node;
      else {
        $client = $node->get('field_tt_call_from')->first();
        if ($client) {
          $this->client = $client->entity;
        }
        else {
          $this->client = FALSE;
        }
      }
      $this->phoneNumber = $phone_number;
    }
    else
      $errors[] = 'У покупателя не заполнен номер телефона';

    if (!$this->user->get('field_tt_user_phone_inner')->getString())
      $errors[] = 'Для текущего пользователя не задан внутренний номер телефона';

    if ($errors)
      return ['errors' => $errors] ;
  }

  public static function getCompany($node) {
    switch ($node->bundle()) {
      case 'contact':
      case 'lead':
        if ($company = $node->get('field_tl_kompaniya')->first()) {
          $company_object = $company->entity;
        }
        break;
      case 'zvonok':
        if ($who_call = $node->get('field_tt_call_from')->first()) {
          switch ($who_call->bundle()) {
            case 'contact':
            case 'lead':
              if ($company = $node->get('field_tl_kompaniya')->first()) {
                $company_object = $company->entity;
              }
              break;
            case 'kompanii':
              $company_object = $who_call;
              break;
          }
        }
        break;
    }
    return $company_object ?? FALSE;
  }
}
