<?php

namespace Drupal\trinion_tel\Plugin\rest\resource;

use Bloatless\WebSocket\PushClient;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\node\Entity\Node;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\user\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Represents TelephonyExternalCallFinish records as resources.
 *
 * @RestResource (
 *   id = "trinion_tel_telephonyexternalcallfinish",
 *   label = @Translation("TelephonyExternalCallFinish"),
 *   uri_paths = {
 *     "create" = "/rest/telephony.externalcall.finish",
 *   }
 * )
 *
 * @DCG
 * This plugin exposes database records as REST resources. In order to enable it
 * import the resource configuration into active configuration storage. You may
 * find an example of such configuration in the following file:
 * core/modules/rest/config/optional/rest.resource.entity.node.yml.
 * Alternatively you can make use of REST UI module.
 * @see https://www.drupal.org/project/restui
 * For accessing Drupal entities through REST interface use
 * \Drupal\rest\Plugin\rest\resource\EntityResource plugin.
 */
class TelephonyExternalCallFinishResource extends ResourceBase {

  /**
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var Node
   */
  protected $node;

  /**
   * @var User
   */
  protected $user;

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats,  LoggerInterface $logger, EntityTypeManager $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger, $entity_type_manager);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('logger'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Responds to POST requests and saves the new record.
   *
   * @param mixed $data
   *   Data to write into the database.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function post($data) {
    \Drupal::logger('trinion_tel')->log(1, json_encode($data));
    if ($response = $this->validate($data)) {
      return new ModifiedResourceResponse($response, 200);
    }

    /** @var Node $call */
    $failed_reason = $data['FAILED_REASON'] ?? 200;
    $record_url = $data['RECORD_URL'] ?? '';
    $duration = $data['DURATION'] ?? '';
    $uid = !empty($this->user) ? $this->user->id() : FALSE;
    $call = \Drupal::service('trinion_tel.telephony')->closeCallEntity($data['CALL_ID'], $duration, $failed_reason, $record_url, $uid, $data['STATUS_CODE']);
    if ($call) {
      $response = [
        'CALL_ID' => $call->id(),
      ];
    }
    $pushClient = new PushClient(realpath(__DIR__ . '/../../../../wserver/phpwss.sock'));
    $pushClient->sendToApplication('ws', [
      'action' => 'CallFinish',
      'data' => [
        'uid' => $this->user->id(),
      ],
    ]);
    return new ModifiedResourceResponse($response, 200);
  }

  /**
   * Validate incoming data
   * @param $data
   */
  public function validate($data) {
    $errors = [];
    if (empty($data['TOKEN']))
      $errors[] = 'TOKEN is required';
    elseif ($data['TOKEN'] != \Drupal::config('trinion_tel.settings')->get('trinion_tel_api_token'))
      $errors[] = 'TOKEN is invalid';
    if (empty($data['CALL_ID']))
      $errors[] = 'CALL_ID is required';
    if (empty($data['STATUS_CODE']))
      $errors[] = 'STATUS_CODE is required';

    if (empty($data['USER_PHONE_INNER']));
    // $errors[] = 'USER_PHONE_INNER is required';
    else {
      $query = \Drupal::entityQuery('user')
        ->condition('field_tt_user_phone_inner', $data['USER_PHONE_INNER']);
      $uids = $query->accessCheck()->execute();
      if ($uids) {
        $this->user = $this->entityTypeManager->getStorage('user')->load(key($uids));
      }
    }

    if ($errors)
      return $errors;

    $node = $this->entityTypeManager->getStorage('node')->load($data['CALL_ID']);
    if ($node) {
      $this->node = $node;
    }
    else {
      $errors[] = "Call id {$data['CALL_ID']} not exist";
    }

    return $errors;
  }

}
