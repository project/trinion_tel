<?php

namespace Drupal\trinion_tel\Plugin\rest\resource;

use Bloatless\WebSocket\PushClient;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\node\Entity\Node;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\user\Entity\User;
use Drupal\trinion_tel\Helper;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Represents TelephonyExternalCallShow records as resources.
 *
 * @RestResource (
 *   id = "trinion_tel_telephonyexternalcallshow",
 *   label = @Translation("TelephonyExternalCallShowResource"),
 *   uri_paths = {
 *     "create" = "/rest/telephony.externalcall.show",
 *   }
 * )
 *
 * @DCG
 * This plugin exposes database records as REST resources. In order to enable it
 * import the resource configuration into active configuration storage. You may
 * find an example of such configuration in the following file:
 * core/modules/rest/config/optional/rest.resource.entity.node.yml.
 * Alternatively you can make use of REST UI module.
 * @see https://www.drupal.org/project/restui
 * For accessing Drupal entities through REST interface use
 * \Drupal\rest\Plugin\rest\resource\EntityResource plugin.
 */
class TelephonyExternalCallShowResource extends ResourceBase {

  /**
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var Node
   */
  protected $call;

  /**
   * @var User
   */
  protected $user;

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats,  LoggerInterface $logger, EntityTypeManager $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger, $entity_type_manager);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('logger'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Responds to POST requests and saves the new record.
   *
   * @param mixed $data
   *   Data to write into the database.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function post($data) {
    \Drupal::logger('trinion_tel')->log(1, json_encode($data));
    if ($response = $this->validate($data)) {
      return new ModifiedResourceResponse($response, 200);
    }

    if ($this->call) {
      $response = [
        'CALL_ID' => $this->call->id(),
      ];
      $pushClient = new PushClient(realpath(__DIR__ . '/../../../../wserver/phpwss.sock'));
      $client = $this->call->get('field_tt_call_from')->first();
      if ($client) {
        $client = $client->entity;
      }
      if (!empty($client)) {
        $company = TelephonyStartCall::getCompany($client);
        if ($company)
          $company_id = $company->id();
        elseif ($client->bundle() == 'kompanii') {
          $company_id = $client->id();
        }
      }

      $this->call->uid = $this->user->id();
      $this->call->save();
      $pushClient->sendToApplication('ws', [
        'action' => 'NewCallPopup',
        'data' => [
          'title' => 'Входящий звонок',
          'type' => 'incoming',
          'callId' => $this->call->id(),
          'clientName' => $client ? $client->label() : '',
          'companyName' => $company ? $company->label() : '',
          'companyId' => $company_id ?? '',
          'clientId' => $client ? $client->id() : FALSE,
          'uid' => $this->user->id(),
          'phoneNumber' => $this->call->get('field_tl_nomer_telefona')->getString(),
        ],
      ]);
    }
    return new ModifiedResourceResponse($response, 200);
  }

  /**
   * Validate incoming data
   * @param $data
   */
  public function validate($data) {
    $errors = [];
    if (empty($data['TOKEN']))
      $errors[] = 'TOKEN is required';
    elseif ($data['TOKEN'] != \Drupal::config('trinion_tel.settings')->get('trinion_tel_api_token'))
      $errors[] = 'TOKEN is invalid';

    if (empty($data['USER_PHONE_INNER']))
      $errors[] = 'USER_PHONE_INNER is required';
    else {
      $query = \Drupal::entityQuery('user')
        ->condition('field_tt_user_phone_inner', $data['USER_PHONE_INNER']);
      $uids = $query->accessCheck()->execute();
      if ($uids) {
        $this->user = $this->entityTypeManager->getStorage('user')->load(key($uids));
      }
      if (empty($this->user))
        $errors[] = "User with inner number {$data['USER_PHONE_INNER']} not exist";
    }
    if (empty($data['CALL_ID']))
      $errors[] = 'CALL_ID is required';
    else {
      $this->call = $this->entityTypeManager->getStorage('node')->load($data['CALL_ID']);
      if (empty($this->call))
        $errors[] = "Call with id {$data['CALL_ID']} not exist";
    }

    if ($errors)
      return ['errors' => $errors] ;
  }

}
