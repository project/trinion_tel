<?php

namespace Drupal\trinion_tel\Plugin\rest\resource;

use Bloatless\WebSocket\PushClient;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\node\Entity\Node;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\user\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Represents TelephonyExternalCallRegister records as resources.
 *
 * @RestResource (
 *   id = "trinion_tel_telephonyexternalcallregister",
 *   label = @Translation("TelephonyExternalCallRegister"),
 *   uri_paths = {
 *     "create" = "/rest/telephony.externalcall.register",
 *   }
 * )
 *
 * @DCG
 * This plugin exposes database records as REST resources. In order to enable it
 * import the resource configuration into active configuration storage. You may
 * find an example of such configuration in the following file:
 * core/modules/rest/config/optional/rest.resource.entity.node.yml.
 * Alternatively you can make use of REST UI module.
 * @see https://www.drupal.org/project/restui
 * For accessing Drupal entities through REST interface use
 * \Drupal\rest\Plugin\rest\resource\EntityResource plugin.
 */
class TelephonyExternalCallRegisterResource extends ResourceBase {

  /**
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var Connection
   */
  protected $connection;

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats,  LoggerInterface $logger, EntityTypeManager $entity_type_manager, Connection $connection) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger, $entity_type_manager);
    $this->entityTypeManager = $entity_type_manager;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('logger'),
      $container->get('entity_type.manager'),
      $container->get('database')
    );
  }

  /**
   * Responds to POST requests and saves the new record.
   *
   * @param mixed $data
   *   Data to write into the database.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function post($data) {
    \Drupal::logger('trinion_tel')->log(1, json_encode($data));
    if ($response = $this->validate($data)) {
      return new ModifiedResourceResponse($response, 200);
    }

    if (empty($data['PHONE_NUMBER'])) {
      $phone_number = 'не определен';
      $client_id = NULL;
    }
    else {
      $phone_number = $data['PHONE_NUMBER'];
      $phone = substr($phone_number, strlen($phone_number) - 10, 10);

      $query = $this->connection->select('node__field_tl_nomer_telefona', 'c')
        ->condition('c.field_tl_nomer_telefona_value', "%{$phone}", 'LIKE')
        ->condition('c.bundle', ['kompanii', 'contact', 'lead', ], 'IN');
      $query->addField('c', 'entity_id');
      $contact_id = $query->execute()->fetchField();
      $client = $contact_id ? Node::load($contact_id) : FALSE;
    }

    /** @var Node $call */
    $call = \Drupal::service('trinion_tel.telephony')->createCallEntity(0, $phone_number, $data['TYPE'], $contact_id);
    if ($call) {
      $response = [
        'CALL_ID' => $call->id(),
      ];
    }

    if (!empty($client)) {
      $company = TelephonyStartCall::getCompany($client);
      if ($company)
        $company_id = $company->id();
      elseif ($client->bundle() == 'kompanii') {
        $company_id = $client->id();
      }
    }

    if (!empty($data['SHOW']) && $this->user) {
      $pushClient = new PushClient(realpath(__DIR__ . '/../../../../wserver/phpwss.sock'));
      $pushClient->sendToApplication('ws', [
        'action' => 'NewCallPopup',
        'data' => [
          'title' => 'Исходящий звонок',
          'type' => 'outgoing',
          'callId' => $call->id(),
          'clientName' => $client ? $client->label() : '',
          'companyName' => $company ? $company->label() : '',
          'companyId' => $company_id ?? '',
          'clientId' => $client ? $client->id() : FALSE,
          'uid' => $this->user->id(),
          'phoneNumber' => $call->get('field_tl_nomer_telefona')->getString(),
        ],
      ]);
    }

    return new ModifiedResourceResponse($response, 200);
  }

  /**
   * Validate incoming data
   * @param $data
   */
  public function validate($data) {
    $errors = [];
    if (empty($data['TOKEN']))
      $errors[] = 'TOKEN is required';
    elseif ($data['TOKEN'] != \Drupal::config('trinion_tel.settings')->get('trinion_tel_api_token'))
      $errors[] = 'TOKEN is invalid';
    if (empty($data['TYPE']))
      $errors[] = 'TYPE is required';

    if (empty($data['USER_PHONE_INNER']));
      // $errors[] = 'USER_PHONE_INNER is required';
    else {
      $query = \Drupal::entityQuery('user')
        ->condition('field_tt_user_phone_inner', $data['USER_PHONE_INNER']);
      $uids = $query->accessCheck()->execute();
      if ($uids) {
        $this->user = $this->entityTypeManager->getStorage('user')->load(key($uids));
      }
    }

    if ($errors)
      return ['errors' => $errors] ;
  }

}
