<?php

namespace Drupal\trinion_tel\Plugin\rest\resource;

use Bloatless\WebSocket\PushClient;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\node\Entity\Node;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\user\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Represents TelephonyExternalCallSearchCrmEntities records as resources.
 *
 * @RestResource (
 *   id = "trinion_tel_telephonyexternalcallsearchcrmentities",
 *   label = @Translation("TelephonyExternalCallSearchCrmEntities"),
 *   uri_paths = {
 *     "create" = "/rest/telephony.externalcall.searchCrmEntities",
 *   }
 * )
 *
 * @DCG
 * This plugin exposes database records as REST resources. In order to enable it
 * import the resource configuration into active configuration storage. You may
 * find an example of such configuration in the following file:
 * core/modules/rest/config/optional/rest.resource.entity.node.yml.
 * Alternatively you can make use of REST UI module.
 * @see https://www.drupal.org/project/restui
 * For accessing Drupal entities through REST interface use
 * \Drupal\rest\Plugin\rest\resource\EntityResource plugin.
 */
class TelephonyExternalCallSearchCrmEntitiesResource extends ResourceBase {

  /**
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var Node
   */
  protected $node;

  /**
   * @var Connection
   */
  protected $connection;

  /**
   * Constructs a Drupal\rest\Plugin\rest\resource\EntityResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats,  LoggerInterface $logger, EntityTypeManager $entity_type_manager, Connection $connection) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger, $entity_type_manager, $connection);
    $this->entityTypeManager = $entity_type_manager;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('logger'),
      $container->get('entity_type.manager'),
      $container->get('database')
    );
  }

  /**
   * Responds to POST requests and saves the new record.
   *
   * @param mixed $data
   *   Data to write into the database.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The HTTP response object.
   */
  public function post($data) {
    \Drupal::logger('trinion_tel')->log(1, json_encode($data));
    if ($response = $this->validate($data)) {
      return new ModifiedResourceResponse($response, 200);
    }

    $response = [
      [
        'USER_NAME' => $this->node->label(),
      ],
    ];

    return new ModifiedResourceResponse($response, 200);
  }

  /**
   * Validate incoming data
   * @param $data
   */
  public function validate($data) {
    $errors = [];

    if (empty($data['TOKEN']))
      $errors[] = 'TOKEN is required';
    elseif ($data['TOKEN'] != \Drupal::config('trinion_tel.settings')->get('trinion_tel_api_token'))
      $errors[] = 'TOKEN is invalid';

    if (empty($data['PHONE_NUMBER']))
      $errors[] = 'PHONE_NUMBER is required';
    else {
      $phone = substr($data['PHONE_NUMBER'], strlen($data['PHONE_NUMBER'])-10, 10);
      $query = $this->connection->select('node__field_tl_nomer_telefona', 'c')
        ->condition('c.field_tl_nomer_telefona_value', "%{$phone}", 'LIKE')
        ->condition('c.bundle', ['kompanii', 'contact', 'lead', ], 'IN');
      $query->addField('c', 'entity_id');
      $nid = $query->execute()->fetchField();
      if ($nid) {
        $this->node = $this->entityTypeManager->getStorage('node')->load($nid);
      }
      if (empty($this->node))
        $errors[] = "Client with phone number {$phone} not exist";
    }

    return $errors;
  }

}
