<?php

namespace Drupal\trinion_tel\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\node\Entity\Node;

/**
 * Defines 'trinion_tel_zvonok_to_contact_relation' queue worker.
 *
 * @QueueWorker(
 *   id = "trinion_tel_zvonok_to_contact_relation",
 *   title = "Установление связи между Звонком и Контактом/Компанией/Лидом",
 *   cron = {"time" = 60}
 * )
 */
class ZvonokToContactRelation extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    foreach (Node::loadMultiple($data['zvonki']) as $zvonok) {
      $zvonok->field_tt_call_from = $data['id'];
      $zvonok->save();
    }
  }

}
